/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 15:33:11 by grass-kw          #+#    #+#             */
/*   Updated: 2015/07/01 13:57:30 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H
# include "libft.h"
# include <termios.h>
# include <term.h>
# include <curses.h>
# include <unistd.h>
# include <fcntl.h>
# define UP buffer[0] == 27 && buffer[1] == '[' && buffer[2] == 'A'
# define DOWN buffer[0] == 27 && buffer[1] == '[' && buffer[2] == 'B'
# define RIGHT buffer[0] == 27 && buffer[1] == '[' && buffer[2] == 'C'
# define LEFT buffer[0] == 27 && buffer[1] == '[' && buffer[2] == 'D'
# define SPACE buffer[0] == ' ' && buffer[1] == 0 && buffer[2] == 0
# define RETURN buffer[0] == '\n' && buffer[1] == 0 && buffer[2] == 0

typedef struct		s_clist
{
	char			*content;
	int				x;
	int				y;
	int				current;
	struct s_clist	*prev;
	struct s_clist	*next;
}					t_clist;

typedef struct		s_env
{
	struct s_clist	*clst;
	char			*name_term;
	struct termios	term;
	struct s_clist	*cur;
	int				line;
	int				column;
	int				size;
}					t_env;

void				selection(t_env *e, int ac, char **av);
void				write_list(t_env *e);
t_clist				*ft_clstnew(char *content);
void				ft_clstadd(t_clist **alst, t_clist *elem);
void				ft_clstprint(t_clist **alst);
void				ft_clstdelone(t_clist **alst);
void				ft_clstdel(t_clist **alst);
t_clist				*ft_lastit(t_clist **alst);
t_clist				*ft_beginit(t_clist **alst);
size_t				ft_getsize(t_clist *alst);
void				ft_error(t_env *e, int mode);
int					my_outc(int c);
int					configure_termios(t_env *e);
void				restore_termios(t_env *e);
void				selection(t_env *e, int ac, char **av);
void				list_select(t_env *e);

#endif
