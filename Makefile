#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/10 11:39:32 by grass-kw          #+#    #+#              #
#    Updated: 2015/02/10 11:39:32 by grass-kw         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC = gcc

NAME = ft_select

HEAD = ./libft

IFLAGS = -I ./inc -I ./libft/includes

LIB = ./libft/libft.a

FLAGS = -Wall  -Werror -Wextra -g

SRCDIR = src/

SRC = 	$(SRCDIR)main.c \
		$(SRCDIR)clst_tools.c \
		$(SRCDIR)clst_tools2.c \
		$(SRCDIR)ft_error.c\
		$(SRCDIR)capability.c\
		$(SRCDIR)termios_configuration.c\
		$(SRCDIR)select.c \
		$(SRCDIR)list_select.c \

OBJ = $(SRC:.c=.o)

RM = rm -rf

INC = inc

all: libft $(NAME)

libft:
	make -C $(HEAD)

$(NAME): $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $(NAME) -ltermcap

%.o: %.c
	$(CC) $(FLAGS) $(IFLAGS) -o $@ -c $<

clean:
	$(RM) $(OBJ) $(NAME).dSYM

fclean: clean
	$(RM) $(NAME)
	make fclean -C $(HEAD)

re: fclean all

gdb: $(OBJ)
	$(CC) $(OBJ) $(LIB) -o $(NAME)
	lldb $(NAME) -w

.PHONY: all libft clean fclean re gdb
