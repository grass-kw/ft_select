/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   termios_configuration.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/26 18:13:16 by grass-kw          #+#    #+#             */
/*   Updated: 2015/07/01 14:50:37 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int		configure_termios(t_env *e)
{
	if ((e->name_term = getenv("TERM")) == NULL)
		return (-1);
	if (tgetent(NULL, e->name_term) == ERR)
		return (-1);
	if (tcgetattr(0, &(e->term)) == -1)
		return (-1);
	e->term.c_lflag &= ~(ICANON);
	e->term.c_lflag &= ~(ICANON);
	e->term.c_lflag &= ~(ECHO);
	e->term.c_cc[VMIN] = 1;
	e->term.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSADRAIN, &(e)->term) == -1)
		return (-1);
	e->column = tgetnum("co");
	e->line = tgetnum("li");
	tputs(tgetstr("ti", NULL), 0, my_outc);
	return (0);
}

void	restore_termios(t_env *e)
{
	if (tcgetattr(0, &e->term) == -1)
		return ;
	e->term.c_lflag = (ICANON | ECHO);
	if (tcsetattr(0, 0, &(e)->term) == -1)
			return ;
}
