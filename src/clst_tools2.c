/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clst_tools2.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 17:29:22 by grass-kw          #+#    #+#             */
/*   Updated: 2015/07/01 12:47:08 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_clist		*ft_lastit(t_clist **alst)
{
	t_clist	*tmp;

	if (!alst || !*alst)
		return (NULL);
	tmp = *alst;
	while (tmp->next != NULL && tmp->next != *alst)
		tmp = tmp->next;
	return (tmp);
}

t_clist		*ft_beginit(t_clist **alst)
{
	return (*alst);
}

size_t		ft_getsize(t_clist *alst)
{
	t_clist	*tmp;
	t_clist	*last;
	size_t	size;

	tmp = alst;
	last = ft_lastit(&(alst));
	size = 0;
	while (tmp != last)
	{
		tmp = tmp->next;
		size++;
	}
	return (size + 1);
}
