/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_select.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/01 11:13:01 by grass-kw          #+#    #+#             */
/*   Updated: 2015/07/01 14:53:20 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void	determine_localisation(t_env *e, t_clist *list)
{
	int		current_column;
	int		i;
	t_clist	*tmp;
	t_clist	*last;

	tmp = list;
	last = ft_lastit(&(list));
	current_column = 0;
	i = 0;
	while (tmp != last)
	{
		tmp->x = i % e->column;
		tmp->y = current_column;
		i++;
		if (i == e->line)
			current_column += 20;
		tmp = tmp->next;
	}
	tmp->x = i % e->column;
	tmp->y = current_column;
}

static void	write_choice(t_clist *choice)
{
	if (choice->current == 1)
		tputs(tgetstr("us", NULL), 1, my_outc);
	else
		tputs(tgetstr("ue", NULL), 1, my_outc);
	tputs(tgoto(tgetstr("cm", NULL), choice->y, choice->x), 1, my_outc);
	if (ft_strlen(choice->content) > 15)
		write(1, choice->content, 15);
	else
		write(1, choice->content, ft_strlen(choice->content));
}

static void	create_select(t_clist *lst)
{
	t_clist	*tmp;
	t_clist	*last;

	tmp = lst;
	last = ft_lastit(&lst);
	while (tmp != last)
	{
		write_choice(tmp);
		tmp = tmp->next;
	}
	write_choice(tmp);
	tputs(tgoto(tgetstr("do", NULL), 0, 0), 1, my_outc);
	tputs(tgetstr("al", NULL), 1, my_outc);
}

void		list_select(t_env *e)
{
	e->size = ft_getsize(e->clst);
	e->line = tgetnum("li");
	e->column = tgetnum("co");
	if (e->size > e->line * (e->column / 20)) // on a pas assez d'espace pour ecrire
		ft_putendl("screen is too small");
	else
	{
		determine_localisation(e, e->clst);
		// ft_clstprint(&(e->clst));
		create_select(e->clst);
	}
}
