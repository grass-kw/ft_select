/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clst_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/09 15:43:08 by grass-kw          #+#    #+#             */
/*   Updated: 2015/07/01 13:58:37 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_clist		*ft_clstnew(char *content)
{
	t_clist	*ret;

	if ((ret = (t_clist *)malloc(sizeof(t_clist))) == NULL)
		return (NULL);
	ret->content = ft_strdup(content);
	ret->current = 0;
	ret->next = NULL;
	ret->prev = NULL;
	return (ret);
}

void		ft_clstadd(t_clist **alst, t_clist *elem)
{
	t_clist	*tmp;
	t_clist	*begin;
	t_clist	*last;

	tmp = *alst;
	if (!tmp)
	{
		*alst = elem;
		elem->current = 1;
	}
	else
	{
		begin = ft_beginit(alst);
		last = ft_lastit(alst);
		elem->next = begin;
		elem->prev = last;
		begin->prev = elem;
		last->next = elem;
	}
}

void		ft_clstprint(t_clist **alst)
{
	t_clist	*tmp;
	t_clist	*last;

	tmp = *alst;
	last = ft_lastit(alst);
	while (tmp != last)
	{
		ft_putendl(tmp->content);
		printf("x : %d, y : %d\n", tmp->x, tmp->y);
		tmp = tmp->next;
	}
	ft_putendl(tmp->content);
	printf("x : %d, y : %d\n", tmp->x, tmp->y);
}

void		ft_clstdelone(t_clist **alst)
{
	if (!*alst)
	{
		ft_strclean((*alst)->content);
		(*alst)->prev = NULL;
		(*alst)->next = NULL;
		free(*alst);
		*alst = NULL;
	}
}

void		ft_clstdel(t_clist **alst)
{
	t_clist	*tmp;
	t_clist	*tmp2;
	t_clist	*last;

	tmp = *alst;
	last = ft_lastit(alst);
	while (tmp != last)
	{
		tmp2 = tmp->next;
		ft_clstdelone(alst);
		tmp = tmp2;
	}
}
