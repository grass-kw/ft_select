/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   capability.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/26 17:47:02 by grass-kw          #+#    #+#             */
/*   Updated: 2015/06/30 08:40:47 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int	my_outc(int c)
{
	int fd;

	fd = open("/dev/tty", O_RDWR);
	if (fd == -1)
	{
		write(2, "Open error\n", 11);
		exit(-1);
	}
	write(fd, &c, 1);
	close(fd);
	return (0);
}
