/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   select.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/26 18:21:33 by grass-kw          #+#    #+#             */
/*   Updated: 2015/07/01 15:01:57 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void	init_env(t_env *e)
{
	e->clst = NULL;
}

static void		parsing(t_env *e, char **av, int ac)
{
	int		i;
	t_clist	*elem;

	i = 1;
	while (i < ac)
	{
		elem = ft_clstnew(av[i]);
		ft_clstadd(&(e->clst), elem);
		i++;
	}
}

void		loop(t_env *e)
{
	while (42)
	{
		event()
	}
}

void		selection(t_env *e, int ac, char **av)
{
	(void)ac;
	(void)av;
	init_env(e);
	parsing(e, av, ac);
	if (configure_termios(e) == -1)
		return ;
	// configure_signal();
	list_select(e);
	loop(e);
	restore_termios(e);
	ft_clstdel(&e->clst);
}
