/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: grass-kw <grass-kw@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/10 17:18:50 by grass-kw          #+#    #+#             */
/*   Updated: 2015/06/10 17:43:11 by grass-kw         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void	clear_leaks(t_env *e, int mode)
{
	if (mode == 2)
		ft_clstdel(&(e->clst));
}

void		ft_error(t_env *e, int mode)
{
	if (mode == 2)
	{
		clear_leaks(e, 2);
		exit(0);
	}
}
